mysql -u root -p
# Log into the database as a root user. Press enter once when prompted for a password (this means no password has been set)

CREATE USER 'fernando'@'localhost' IDENTIFIED BY 'powerwalk';
# Creates a user and password for the MySQL database

CREATE DATABASE `wordpress-db`;
# Creates a database called wordpress-db

GRANT ALL PRIVILEGES ON `wordpress-db`.* TO "fernando"@"localhost";
# Grant full privileges for your database to the WordPress user

FLUSH PRIVILEGES;
# Flush the database privileges to pick up all changes

exit
# Exit MySQL